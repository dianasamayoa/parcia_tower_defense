﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletMovement : MonoBehaviour {

	private float bulletSpeed = 12;
	private GameObject target;
	private float damage = 5;


	void Awake ()
	{
		StartCoroutine (DestroyBullet ());

	}
	// Update is called once per frame
	void Update () {
		
		transform.Translate (Vector3.forward * Time.deltaTime * bulletSpeed);
	}

	IEnumerator DestroyBullet()
	{
		yield return new WaitForSeconds (3);
		Destroy (gameObject);
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Enemy") 
		{
			target = other.gameObject;
			target.GetComponent<EnemyScript> ().health -= damage;

			Destroy (this.gameObject); 
		}
	}
}
