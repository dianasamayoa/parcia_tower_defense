﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyWalk : MonoBehaviour {
	private NavMeshAgent agent;
	private GameObject target;
	// Use this for initialization
	void Start () {
		
		agent = GetComponent<NavMeshAgent> ();
		target = GameObject.Find ("RedHexagon");
		agent.SetDestination (target.transform.position);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
