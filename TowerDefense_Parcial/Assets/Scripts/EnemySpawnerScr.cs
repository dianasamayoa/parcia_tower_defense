﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnerScr : MonoBehaviour {

	void Start()
	{
		ui_Data = GameObject.Find ("Canvas").GetComponent<IngameUI> ();
	}
	public enum EnemyTypes
	{
		buggy,
		helicopter,
		hover,
	};
	[System.Serializable]

	public struct Waves
	{

		public EnemyTypes[] enemySelectWave;
	}
	public Waves[] numOfWaves;

	[SerializeField] private GameObject buggyPrefab;
	[SerializeField] private GameObject helicopterPrefab;
	[SerializeField] private GameObject hoverPrefab;
	[SerializeField] private int currentWave = 0;
	[SerializeField] private int currentEnemy = 0;
	[SerializeField] private int timeBetween = 2;
	IngameUI ui_Data;

	public IEnumerator SendWave()
	{
		print (currentWave);
		ui_Data.UpdateCurrentWave(currentWave+1);
		while (currentEnemy < numOfWaves[currentWave].enemySelectWave.Length) 
		{	
			switch (numOfWaves[currentWave].enemySelectWave [currentEnemy]) 
			{   
			case EnemyTypes.buggy: 
				Instantiate (buggyPrefab);
				currentEnemy++;
				break;
			}
			yield return new WaitForSeconds (timeBetween);
		}

		if (currentWave < numOfWaves.Length-1) {
			currentEnemy = 0;
			currentWave++;
			yield return new WaitForSeconds (4);
			StartCoroutine (SendWave());
		}
		if (currentWave == numOfWaves.Length) 
		{
			print ("No more waves to show");
		}
	}

}
