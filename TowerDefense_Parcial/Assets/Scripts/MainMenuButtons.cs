﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuButtons : MonoBehaviour {
    [SerializeField] public GameObject levelsHolderBack;
    [SerializeField] public GameObject mainMenuBTS;


	[HideInInspector] public Animator anim;
   
	void Start () {
		
	}

    public void SelectLevelBT()
    {
		anim = GameObject.Find ("Main Camera").GetComponent<Animator>();

        levelsHolderBack.SetActive(true);
		anim.SetTrigger ("ZoomOutTrigger");
        mainMenuBTS.SetActive(false);
    }
    public void QuitBT()
    {
        Application.Quit();
    }
    public void LoadLevelBT(string levelname)
    {

        SceneManager.LoadScene(levelname);
    }
   
}
