﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IngameUI : MonoBehaviour {

	[SerializeField] private Text currencyCounter;
	[SerializeField] private Text healthCounter;
	[SerializeField] private Text waveCounter;
	[HideInInspector] public int health = 10;
	[HideInInspector] public int currency = 10;
	[HideInInspector] public int currentWave = 0;
	// Use this for initialization
	void Start () {
		healthCounter.text = "" + health;
		currencyCounter.text = "" + currency;
		//StartCoroutine (IncreaseEnergy ());
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void UpdateEnergyUI( int value)
	{
			currency += value;
		currencyCounter.text = "" + currency;
	}
	public void UpdateHealthUI(int value)
	{
		health -= value;
		healthCounter.text = "" + health;
	}
	public void UpdateCurrentWave (int value)
	{
		currentWave = value;
		waveCounter.text = "" + currentWave;
	}

	public IEnumerator IncreaseEnergy()
	{
		
		yield return new WaitForSeconds (2f);
		currency++;
		currencyCounter.text = "" + currency;
		StartCoroutine (IncreaseEnergy ());

	}
}
