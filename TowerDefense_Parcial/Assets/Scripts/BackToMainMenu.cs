﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackToMainMenu : MonoBehaviour {

    [SerializeField] GameObject mainMenuBTS;
    [SerializeField] GameObject levelHolderBack;
	//[SerializeField] GameObject cameraobj;
	[HideInInspector] public Animator anim;


    public void Start()
    {
		
    }
    public void backBT()
    {
		anim = GameObject.Find ("Main Camera").GetComponent<Animator>();
        mainMenuBTS.SetActive(true);
		anim.SetTrigger ("ZoomInTrigger");
		levelHolderBack.SetActive(false);

       
    }

  
}
