﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class turret_follow : MonoBehaviour {

	private GameObject target;
	private bool targetLocked;
	[SerializeField] GameObject turret_top;

	[SerializeField] GameObject bulletPrefab;
	[SerializeField] GameObject spawnBulletPos;
	private float fireTimer = 0.5f ;
	private bool shotReady;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (target != null) 
		{
			if (targetLocked) 
			{
				turret_top.transform.LookAt (target.transform);
				turret_top.transform.Rotate (0, 0, 0);
			}

		}

	}

	void Shoot()
	{	
		Transform bulletClone = Instantiate (bulletPrefab.transform, spawnBulletPos.transform.position, Quaternion.identity);
		bulletClone.transform.rotation = spawnBulletPos.transform.rotation;

	}
		
	void OnTriggerEnter (Collider other)
	{
		
		if (other.tag == "Enemy")
		{
			
			shotReady = true;
			target = other.gameObject;
			targetLocked = true;
		}

			if (shotReady) 
			{
			
				InvokeRepeating ("Shoot", 1f, fireTimer);
			}
				

		}
		

	void OnTriggerExit (Collider other)
	{
		
		if (other.tag == "Enemy") {
			
			CancelInvoke ();
			target = other.gameObject;
			targetLocked = false;
			shotReady = false;

		}
	}
}
