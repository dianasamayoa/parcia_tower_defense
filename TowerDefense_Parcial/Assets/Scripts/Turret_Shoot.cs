﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret_Shoot : MonoBehaviour {

	[SerializeField]  GameObject turret_top;
	private EnemyScript target;
	[SerializeField] GameObject bulletPrefab;
	[SerializeField] GameObject spawnBulletPos;
	private Transform targetTrans;
	[SerializeField] private float range = 6f;
	// Use this for initialization
	void Start () {
		InvokeRepeating ("FindClosestEnemy", 0f, 0.5f);
	}

	// Update is called once per frame
	void Update () {
		//FindClosestEnemy ();
		if (target != null) 
		{

			turret_top.transform.LookAt (target.transform);
			turret_top.transform.Rotate (0, 0, 0);


		}
		if (targetTrans == null) 
		{
			return;
		}
	}
	void FindClosestEnemy ()
	{
			float distanceToClosestEnemy = Mathf.Infinity;
			EnemyScript closestEnemy = null; 
			EnemyScript[] allEnemies = GameObject.FindObjectsOfType<EnemyScript> ();
			foreach (EnemyScript currentEnemy in allEnemies)
		{
				float distanceToEnemy = Vector3.Distance (this.transform.position, currentEnemy.transform.position);
				if (distanceToEnemy < distanceToClosestEnemy) 
			{
					distanceToClosestEnemy = distanceToEnemy;
					closestEnemy = currentEnemy;
					target = closestEnemy;
				
				}

		}
		if (closestEnemy != null && distanceToClosestEnemy <= range) {
			targetTrans = closestEnemy.transform;
			InvokeRepeating ("shoot", 0.1f, 0.6f);
		} else 
		{
			targetTrans = null;
			target = null;
			CancelInvoke ("shoot");
		}
	}
	void shoot ()
	{
		Transform bulletClone = Instantiate (bulletPrefab.transform, spawnBulletPos.transform.position, Quaternion.identity);
		bulletClone.transform.rotation = spawnBulletPos.transform.rotation;
	}

	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere (transform.position, range);
	}
		

}