﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour {

	public float health = 10;
	[SerializeField] GameObject redHexagon;
	float distance;
	IngameUI ui_data;

	// Use this for initialization
	void Awake() {
		
		ui_data = GameObject.Find ("Canvas").GetComponent<IngameUI> ();
	}
	
	// Update is called once per frame
	void Update () {
		
		if (health <= 0)
		{
			Die ();
		}
		distance = Vector3.Distance (this.gameObject.transform.position, redHexagon.transform.position);
		if (distance < 2 && distance > 1.95f) 
		{	
				ReachedGoal ();
				Destroy (gameObject, 1);
			
		}
	}

	void Die()
	{
		
		Destroy (this.gameObject);
	}

	void ReachedGoal(){
	
		ui_data.UpdateHealthUI (1);
	}

}
