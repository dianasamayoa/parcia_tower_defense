﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

	[SerializeField] private float panSpeed = 20.0f;
	[SerializeField] private float panBorderThicknes = 5f;

	// Update is called once per frame
	void Update ()
	{
		

		Vector3 pos = transform.position;

		if (Input.mousePosition.y >= Screen.height - panBorderThicknes && pos.z < -15) {

			pos.z += panSpeed * Time.deltaTime;	
		}

		if (Input.mousePosition.y <= panBorderThicknes && pos.z > -50) {

			pos.z -= panSpeed * Time.deltaTime;	
		}
		if (Input.mousePosition.x >= Screen.width - panBorderThicknes && pos.x < 2) {

			pos.x += panSpeed * Time.deltaTime;	
		}
		if (Input.mousePosition.x <= panBorderThicknes && pos.x > -20) {

			pos.x -= panSpeed * Time.deltaTime;	
		}
		transform.position = pos;


	}
}
