﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
	[SerializeField] private GameObject machiegunPrefab;
	//[SerializeField] private GameObject missilePrefab;
	//[SerializeField] private GameObject sniperPrefab;
	[SerializeField] private GameObject holdingWeapon;
	EnemySpawnerScr enemySpawner;
	IngameUI ui_data;

	// Use this for initialization
	void Start () {
		ui_data = GetComponent<IngameUI>();
		enemySpawner =GameObject.Find ("EnemySpawner").GetComponent<EnemySpawnerScr> ();
	}
	
	// Update is called once per frame
	void Update () {
		
		if (holdingWeapon != null) 
		{
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hitInfo;
			if (Physics.Raycast (ray, out hitInfo)) 
			{
				holdingWeapon.transform.position = hitInfo.point;

				if (Input.GetMouseButtonDown (0)) 
				{
					if (hitInfo.transform.CompareTag("Placement"))
					{
						holdingWeapon = null;
						Destroy (hitInfo.collider);
					}
				}
			}
		}
	}

	public void CreateWeapon( string weaponType)
	{
		if (holdingWeapon != null) {
		
			return;
		}
		if (ui_data.currency >= 4) {

			switch (weaponType) {
			case "gun":
				holdingWeapon = Instantiate (machiegunPrefab);
				ui_data.UpdateEnergyUI (-4);
				break;
			}
		}

	}
	public void StartWaves()
	{
		enemySpawner.StartCoroutine (enemySpawner.SendWave());
		ui_data.StartCoroutine (ui_data.IncreaseEnergy ());

	}
}
